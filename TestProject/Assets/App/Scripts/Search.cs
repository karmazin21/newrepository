﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Search : MonoBehaviour
{
    [SerializeField]
    private GameObject _skin;
    [SerializeField]
    public Toggle _skinToggle;
    public ContentList Content;
    [SerializeField]
    private Text numberSkin;
    
    //public int _savePrefs = 1;


    void Awake()
    {
        //_savePrefs = PlayerPrefs.GetInt("saveSkin",0);
    }

    void Start()
    {
        _skinToggle.onValueChanged.AddListener(transferToggle);
        
        /* if(_savePrefs == 1)
        {
            _skinToggle.isOn = true;
        } else if(_savePrefs == 0)
        {
            _skinToggle.isOn = false;
        }*/
    }
    public void transferToggle(bool _skinToggle)
    {   
        if(_skinToggle == true)
        {
            transferGameObject(_skin,_skinToggle);
            //savePrefs(1);
        } else 
        {
            removeGameObject(_skin,_skinToggle);
            //removePrefs(0);
        }
    }
    public void transferGameObject(GameObject _skin, bool _skinToggle)
    {
        Content.GetComponent<ContentList>().Objects.Add(_skin);
        Content.GetComponent<ContentList>().Toggles.Add(_skinToggle);        
        
        
    }
    private void removeGameObject(GameObject _skin, bool _skinToggle)
    {
        Content.GetComponent<ContentList>().Objects.Remove(_skin);
        Content.GetComponent<ContentList>().Toggles.Remove(_skinToggle);
    }

   /* public void savePrefs(int _savePrefs)
	{
		_savePrefs = 1;
        PlayerPrefs.SetInt("saveSkin",_savePrefs);
	}
    public void removePrefs(int _savePrefs)
	{
		_savePrefs = 0;
        PlayerPrefs.SetInt("saveSkin",_savePrefs);
	}*/
}
