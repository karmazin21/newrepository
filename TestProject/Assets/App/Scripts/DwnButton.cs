﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwnButton : MonoBehaviour
{
    public GameObject[] Buttons;

    public GameObject p1;
    public GameObject p2;
    public GameObject p3;
    public GameObject p4;
    public GameObject p5;
    public GameObject p6;
    public GameObject p7;
    public GameObject p8;

    public string url;

    void Start()
    {
       
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OffDwn()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
        }
    }

    public void OpenLikePage()
    {
        Application.OpenURL(url);
    }

    public void Page1()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
            
        }
        p1.SetActive(true);
        
    }
    public void Page2()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
        }
        p2.SetActive(true);
    }
    public void Page3()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
        }
        p3.SetActive(true);
    }
    public void Page4()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
        }
        p4.SetActive(true);
    }
    public void Page5()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
        }
        p5.SetActive(true);
    }
    public void Page6()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
        }
        p6.SetActive(true);
    }
    public void Page7()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
        }
        p7.SetActive(true);
    }
    public void Page8()
    {
        foreach(GameObject Button in Buttons)
        {
            Button.SetActive(false);
        }
        p8.SetActive(true);
    }


}
