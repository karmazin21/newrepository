﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ImageLoader : MonoBehaviour
{
    public string url = "https://i.pinimg.com/originals/9e/1d/d6/9e1dd6458c89b03c506b384f537423d9.jpg";
    public Image Image;

    
void Start(){    
    StartCoroutine(DownloadImage(url));
}

 IEnumerator DownloadImage(string MediaUrl)
{   
    UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);

    yield return request.SendWebRequest();
    
    if(request.isNetworkError || request.isHttpError){
        Debug.Log(request.error);
    }
    else
    {
        //rawImage.GetComponent <UnityEngine.UI.Image>().overrideSprite = ((DownloadHandlerTexture) request.downloadHandler).texture;
        Texture2D webTexture = ((DownloadHandlerTexture)request.downloadHandler).texture as Texture2D;
        Sprite webSprite = SpriteFromTexture2D (webTexture);
        Image.sprite = webSprite;
    }
}
 Sprite SpriteFromTexture2D(Texture2D texture) {

  return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0, 0), 100);
 }


 }