﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContentList : MonoBehaviour
{   
    public GameObject[] allSkins = new GameObject[100];
    
    [SerializeField]
    private Toggle myFavoriteToggle;
    public List<GameObject> Objects;
    public List<bool> Toggles;

    private int b = 1;
    public int saveCount;  

    public int i = 0; 
    void Awake()
    {
        for(int c = 1; c < allSkins.Length+1; c++ )
         {
             allSkins[c-1] = GameObject.Find("btnSkin ("+c+")");
             
         }
    }
    
    
    void Start()
     {

         foreach(GameObject skin in allSkins)
         {
             skin.GetComponentInChildren<Text>().text = b++.ToString();
             
         }
         
         myFavoriteToggle.onValueChanged.AddListener(myFavoriteOnClick);

     }

     private void myFavoriteOnClick(bool MenuToggle)
     {
         if(MenuToggle != false)
         {
            offAllSkins();
            activeFavoriteSkin();
         } else if(MenuToggle !=true)
         {
             favoriteListClose();
         }
     }
     private void favoriteListClose()
     {
         foreach(GameObject skin in allSkins)
         {   
           
           skin.gameObject.SetActive(true);
             
         }
     }

     private void offAllSkins()
     {
         foreach(GameObject skin in allSkins)
         {
         
             skin.gameObject.SetActive(false);
         
         }
     }
     private void activeFavoriteSkin()
     {
         foreach(GameObject Object in Objects)
         {  
            Object.SetActive(true);
         }
     }
     public void saveFavoriteCount()
     {
             for(i = 0; i < Toggles.Count; i++) 
             {
              //PlayerPrefs.SetInt(i.ToString, (bool)Toggles[i].GetComponent<Toggle>().IsActive(true));
             }

            
     }
    
   
}
