﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using UnityEngine.Networking;


public class DownloadFile : MonoBehaviour
{
        //public string web = "DCIM/album/filenameFormatted";
        public string Filename = ".png";

        public string url ;

     public void publicToGetFile()
        {
           StartCoroutine(this.GetText("fileName"));
        }

    public IEnumerator GetText (string fileName)
    {

        //var url = "https://i.ibb.co/qgbtSXL/1.png";
        using (var www = UnityWebRequest.Get(url))
        {
            yield return www.SendWebRequest();

            if(www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
              /*   var savePath = string.Format("{0}/{1}.jpg", web, fileName);
                System.IO.File.WriteAllText(savePath, Filename);
                //Debug.Log ("Hello:" + savePath);
                NativeGallery.SaveImageToGallery(savePath,"Album Name", System.IO.Path.GetFileName(savePath));*/
                NativeGallery.SaveImageToGallery(www.downloadHandler.data,"Album Name", "image {0}.png");
            }

        }
   }
}
